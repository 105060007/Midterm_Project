// 在板裡面顯示有哪些貼文
function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {     // Check user login
            user_email = user.email;
            menu.innerHTML = "<a href='self.html'><span class='dropdown-item'>" + user.email + "</span></a><a href='powen.html'><span class='dropdown-item'>POST</span></a><span class='dropdown-item' id='logout-btn'>Logout</span>";
            var logout_button = document.getElementById('logout-btn');

            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
        } else {    // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    var str = location.pathname;
    var str1 = str.split('/');
    var str2 = str1[1].split('.');
    var post_cate = str2[0];

    // The html code for post
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>\n";

    var postsRef = firebase.database().ref(post_cate + '_com_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                total_post[total_post.length] = "<a href='post_"+post_cate+".html?tid="+childSnapshot.key+ "'>" + str_before_username + childData.email + "</strong>" + childData.title + str_after_content;
                first_count += 1;
            });
            document.getElementById('post_list').innerHTML = total_post.join('');

            //add listener
            postsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.title + str_after_content;
                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });
            if(first_count==0 && second_count==0) document.getElementById('post_list').innerHTML = "趕快PO篇廢文!!!";
        })
        .catch(e => console.log(e.message));
}

window.onload = function () {
    init();
};