// 在powen.html裡把東西存到Database
function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {     // Check user login
            user_email = user.email;
            menu.innerHTML = "<a href='self.html'><span class='dropdown-item'>" + user.email + "</span></a><a href='powen.html'><span class='dropdown-item'>POST</span></a><span class='dropdown-item' id='logout-btn'>Logout</span>";
            var logout_button = document.getElementById('logout-btn');

            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });

            console.log(user_email);

            post_btn = document.getElementById('post_btn');
            post_tit = document.getElementById('title');        // 文章標題
            post_txt = document.getElementById('content');      // 文章內容
            post_cate = document.getElementById('category');    // 分類

            post_btn.addEventListener('click', function () {
                if (post_tit.value != "" && 　post_txt.value != "") {

                    var newpostref = firebase.database().ref(post_cate.value + '_com_list').push();
                    newpostref.set({
                        email: user_email,
                        cate: post_cate.value,
                        title: post_tit.value,
                        text: post_txt.value
                    });

                    alert("成功PO文啦~");
                    post_tit = "";
                    post_txt = "";
                    
                }
            })
        } else {    // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
        }
    });
}

window.onload = function () {
    init();
};