
var selectedFile;

function init(){
    var selfimg = document.getElementById('selfimg');
    var username = document.getElementById('username');
    var email = document.getElementById('Email');

    var storageRef = firebase.storage().ref();

    var user = firebase.auth().currentUser;

    var user_email = '';

    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<a href='self.html'><span class='dropdown-item'>" + user.email + "</span></a><a href='powen.html'><span class='dropdown-item'>POST</span></a><span class='dropdown-item' id='logout-btn'>Logout</span>";
            
            email.innerHTML += user_email;

            var logout_button = document.getElementById('logout-btn');

            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });

            storageRef.child(user.email).getDownloadURL().then(function (url) {
                document.getElementById('selfimg').src = url;
            }).catch(function (error) {
                console.log(error.message);
            });

            /*user.updateProfile({
                displayName: userName,
                photoURL: userPhotoURL,                
            });*/
        

        } else {        // if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
        }
    });


    // Upload
    var btnUL = document.getElementById('btnUL');

    btnUL.addEventListener('click', e => {
        var fileRef = storageRef.child(user.email);

        fileRef.put(selectedFile).then(function (snapshot) {
            document.getElementById('selfimg').src = URL.createObjectURL(selectedFile);
            console.log('Uploaded a blob or file!');
        }).catch(function (error) {
            console.log(error.message);
        });
});

}


window.onload = function () {
  init();
};