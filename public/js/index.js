function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<a href='self.html'><span class='dropdown-item'>" + user.email + "</span></a><a href='powen.html'><span class='dropdown-item'>POST</span></a><span class='dropdown-item' id='logout-btn'>Logout</span>";
            
            var logout_button = document.getElementById('logout-btn');

            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });
}

window.onload = function () {
    init();
};