//點文章時顯示在post.html
function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<a href='self.html'><span class='dropdown-item'>" + user.email + "</span></a><a href='powen.html'><span class='dropdown-item'>POST</span></a><span class='dropdown-item' id='logout-btn'>Logout</span>";

            var logout_button = document.getElementById('logout-btn');

            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            // document.getElementById('post_list').innerHTML = "";
        }
    });

    // 到底是哪ㄍ板~~
    var str = location.pathname;
    var str1 = str.split('/');
    var str2 = str1[1].split('.');
    var str3 = str2[0].split('_');
    var post_cate = str3[1];

    var tid = getValue("tid");
    console.log(tid);

    var post_title = document.getElementById('title');
    var post_content = document.getElementById('content');
    var post_email = document.getElementById('Email');
    var post_img = document.getElementById('postimg');

    var storageRef = firebase.storage().ref();

    // 顯示貼文內容
    firebase.database().ref('/' + post_cate + '_com_list/' + tid).once('value').then(function (snapshot) {
        post_title.innerHTML = "<h5>" + snapshot.val().title + "</h5>";
        post_content.innerHTML = snapshot.val().text;
        post_email.innerHTML += snapshot.val().email;

        storageRef.child(snapshot.val().email).getDownloadURL().then(function (url) {
            post_img.src = url;
        }).catch(function (error) {
            console.log(error.message);
        });
    });

    post_btn = document.getElementById('post_btn');
    comment = document.getElementById('comment');

    post_btn.addEventListener('click', function () {
        if (comment!="") {
            var newpostref = firebase.database().ref(post_cate + '_com_list/' + tid + '/post').push();
            newpostref.set({
                email: user_email,
                comment: comment.value
            });

            alert("成功PO文啦~");
            post_tit = "";
            post_txt = "";
        }
    })
    console.log(post_cate);

    // The html code for post
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><p class='media-body pb-3 mb-0 small lh-125'>";
    var str_after_content = "</p></div>\n";

    var postsRef = firebase.database().ref(post_cate + '_com_list/' + tid + '/post');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                total_post[total_post.length] = str_before_username + childData.email + ": " + childData.comment + str_after_content;
                first_count += 1;
            });
            document.getElementById('comment_list').innerHTML = total_post.join('');

            //add listener
            postsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    total_post[total_post.length] = str_before_username + childData.email + ": " + childData.comment + str_after_content;
                    document.getElementById('comment_list').innerHTML = total_post.join('');
                }
            });
            if(first_count==0 && second_count==0){
                document.getElementById('comment_list').innerHTML = "趕快來留言QQ拜託託QQ";
            }
        })
        .catch(e => console.log(e.message));
    
}

function getValue(varname) {
    var url = window.location.href;
    var qparts = url.split("?");
    if (qparts.length == 0) { return ""; }
    var query = qparts[1];
    var vars = query.split("&amp;");
    var value = "";
    for (i = 0; i < vars.length; i++) {
        var parts = vars[i].split("=");
        if (parts[0] == varname) {
            value = parts[1];
            break;
        }
    }
    value = unescape(value);
    value.replace(/\+/g, " ");
    return value;
}

window.onload = function () {
    init();
}