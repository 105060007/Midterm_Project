# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [Forum]
* Key functions (add/delete)
    1. post list page/ post page
    2. PO文(在dropdown按POST)、在下面留言
    3. user page
* Other functions (add/delete)
    1. 分板(動漫、遊戲、...)
    2. 在PO文留言時顯示留言者的email
    3. 在各板中的貼文可以選擇回到板上
    4. 點左上角的OTAKU Forum可以回到主頁(index.html)

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|N|
|Other functions|1~10%|Y|

## Website Detail Description
* Membership Mechanism
    1. Sign in / Sign up
    2. 可用信箱註冊登入
* Database read/write
    1. 儲存貼文、留言、個人照片
* RWD
    1. 長寬設定用% 及 Lab06的offcanvas.js
* Key functions (add/delete)
    1. post list page/ post page
    2. PO文(在dropdown按POST)、在下面留言
    3. 個人頁面(在dropdown按自己的信箱，可設定照片)
* Advanced components
    1. Sign Up/In with Google
    2. CSS animation(在剛進index.html可看到各板變色)
* Other functions (add/delete)
    1. 分板(動漫、遊戲、...)
    2. 在PO文留言時顯示留言者的email
    3. 在各板中的貼文可以選擇回到板上
    4. 點左上角的OTAKU Forum可以回到主頁(index.html)
    5. 個人頁面，可以上傳圖片當頭像
    6. PO文有PO文者頭像(沒設定頭像的用default的self.jpg)、PO文者Email